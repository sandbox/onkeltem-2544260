<?php

/**
 * @file
 * Administrative management forms for CSS libraries.
 */

/**
 * Form builder function for page callback.
 */
function css_libraries_builtin_form($form, &$form_state) {
  return array();
}

function css_libraries_builtin_form_submit($form, &$form_state) {
  return;
}

/**
 * Form builder function for page callback.
 */
function css_libraries_custom_form($form, &$form_state) {
  // Weights range from -delta to +delta, so delta should be at least half
  // of the amount of blocks present. This makes sure all blocks in the same
  // region get an unique weight.
  $custom = variable_get('css_libraries_custom_libraries', array());
  $weight_delta = max(round(count($custom) / 2), 5);
  $path = drupal_get_path('module', 'css_libraries');
  $form = array(
    '#attached' => array(
      'css' => array($path . '/css/css_libraries.admin.css',),
    ),
  );
  $group_options = _css_libraries_groups() + array('disabled' => t('Disabled'));
  $form['groups'] = array(
    '#type' => 'value',
    '#value' => $group_options,
  );
  $form['libraries'] = array();
  $form['#header'] = array('data' => array('data' => t('Description'), 'colspan' => 2), t('Group'), array('data' => t('Weight'), 'class' => array('library-weight')), t('Every page'), t('Media types'), t('Source'), array('data' => t('Operations'), 'colspan' => 2));

  $form['#tree'] = TRUE;
  $block_settings = variable_get('css_libraries_block_settings', array());

  foreach ($custom as $key => $library) {
    $blocks = array();
    // Only show block settings when relevant:
    if ($library['group'] == 'disabled') {
      foreach ($block_settings as $module => $deltas) {
        foreach ($deltas as $delta => $ids) {
          if (in_array($key, $ids)) {
            $module_blocks = module_invoke($module, 'block_info');
            if (!empty($module_blocks[$delta])) {
              $blocks[] = $module_blocks[$delta]['info'];
            }
          }
        }
      }
    }

    $form['libraries'][$key]['name'] = array(
      '#markup' => check_plain($library['name']),
    );

    $form['libraries'][$key]['extra'] = array(
      '#type' => 'item',
      '#description' => $blocks ?  ' ' . t('Enabled for blocks: %blocks', array('%blocks' => implode(', ', $blocks))) : '',
    );

    $form['libraries'][$key]['weight'] = array(
      '#type' => 'weight',
      '#default_value' => $library['weight'],
      '#delta' => $weight_delta,
      '#title_display' => 'invisible',
      '#title' => t('Weight for @library', array('@library' => $library['name'])),
      '#attributes' => array('class' => array('library-weight', 'library-weight-' . $library['group'])),
    );
    $form['libraries'][$key]['group'] = array(
      '#type' => 'select',
      '#default_value' => $library['group'],
      '#title_display' => 'invisible',
      '#title' => t('Region for @library', array('@library' => $library['name'])),
      '#options' => $group_options,
      '#attributes' => array('class' => array('library-region-select', 'library-region-' . $library['group'])),
    );
    $form['libraries'][$key]['every_page'] = array(
      '#type' => 'markup',
      '#markup' => $library['every_page'] ? t('Yes') : t('No'),
    );
    $form['libraries'][$key]['media_types'] = array(
        '#type' => 'markup',
        '#markup' => !empty($library['media_types']) ? implode(', ', $library['media_types']) : 'all',
    );
    // Add brackets around the disabled one.
    $form['libraries'][$key]['group']['#options']['disabled'] = '<' . $form['libraries'][$key]['group']['#options']['disabled'] . '>';
    $library_types = array('external' => t('External'), 'internal' => t('Internal'), 'file' => t('File'));
    $form['libraries'][$key]['type'] = array(
      '#type' => 'markup',
      '#markup' => $library_types[$library['type']],
    );
    $form['libraries'][$key]['edit'] = array(
      '#type' => 'link',
      '#title' => t('edit'),
      '#href' => 'admin/config/system/css-libraries/custom/' . $key . '/edit',
    );
    $form['libraries'][$key]['delete'] = array(
      '#type' => 'link',
      '#title' => t('delete'),
      '#href' => 'admin/config/system/css-libraries/custom/' . $key . '/delete',
   );
  }

  $form['actions'] = array(
    '#tree' => FALSE,
    '#type' => 'actions',
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Default theme function for css_libraries_custom_form().
 *
 * @see block module drag and drop table construction.
 */
function theme_css_libraries_custom_form($variables) {
  $form = $variables['form'];

  $listing = array();
  $i = 0;
  foreach (element_children($form['libraries']) as $key) {
    $i++;
    $idx = $form['libraries'][$key]['weight']['#default_value'] . '.0' . $i;
    $group = $form['libraries'][$key]['group']['#default_value'];
    $listing[$group][$idx]['class'] = array("draggable");
    $listing[$group][$idx]['data'][] = drupal_render($form['libraries'][$key]['name']);
    $listing[$group][$idx]['data'][] = drupal_render($form['libraries'][$key]['extra']);
    $listing[$group][$idx]['data'][] = drupal_render($form['libraries'][$key]['group']);
    $listing[$group][$idx]['data'][] = drupal_render($form['libraries'][$key]['weight']);
    $listing[$group][$idx]['data'][] = drupal_render($form['libraries'][$key]['every_page']);
    $listing[$group][$idx]['data'][] = drupal_render($form['libraries'][$key]['media_types']);
    $listing[$group][$idx]['data'][] = drupal_render($form['libraries'][$key]['type']);
    $listing[$group][$idx]['data'][] = drupal_render($form['libraries'][$key]['edit']);
    $listing[$group][$idx]['data'][] = drupal_render($form['libraries'][$key]['delete']);
  }
  $rows = array();
  foreach ($form['groups']['#value'] as $group => $title) {
    $populated_class = !empty($listing[$group]) ? 'region-populated' : 'region-empty';
    $top = array(
      array('data' => array(array('data' => $title, 'colspan' => 8)), "no_striping" => TRUE, 'class' => array('region-title', 'region-list-' . $group)),
      array('data' => array(array('data' => t('No libraries in this group'), 'colspan' => 8)), "no_striping" => TRUE, 'class' => array('region-message', 'region-' . $group . '-message', $populated_class)),
    );
    $rows = array_merge($rows, $top);
    if (!empty($listing[$group])) {
      ksort($listing[$group], SORT_NUMERIC);
      $rows = array_merge($rows, array_values($listing[$group]));
    }
  }
  $table_attributes = array('id' => 'css-libraries-custom');
  if ($listing) {
    // Add table css if there is anything to drag-n-drop.
    drupal_add_js('misc/tableheader.js');
    drupal_add_js(drupal_get_path('module', 'css_libraries') . '/css_libraries.js');
    foreach ($form['groups']['#value'] as $group => $title) {
      drupal_add_tabledrag('css-libraries-custom', 'match', 'sibling', 'library-region-select', 'library-region-' . $group, NULL, FALSE);
      drupal_add_tabledrag('css-libraries-custom', 'order', 'sibling', 'library-weight', 'library-weight-' . $group);
    }
  }
  else {
    // A CSS class to hide the weight column when the table is empty.
    $table_attributes['class'] = array('empty');
  }
  return theme('table', array('header' => $form['#header'], 'rows' => $rows, 'attributes' => $table_attributes)) . drupal_render_children($form);
}

/**
 * Form submission handler for the custom libraries form.
 *
 * @see css_libraries_custom_form()
 */
function css_libraries_custom_form_submit($form, &$form_state) {
  $custom = variable_get('css_libraries_custom_libraries', array());

  foreach ($form_state['values']['libraries'] as $key => $library) {
    $custom[$key]['group'] = $library['group'];
    $custom[$key]['weight'] = $library['weight'];
  }
  variable_set('css_libraries_custom_libraries', $custom);
  drupal_set_message(t('The css library settings have been updated.'));
}

/**
 * Form builder function for page callback.
 */
function css_libraries_delete_form($form, &$form_state, $library = array()) {
  $form = array();
  $form['#library'] = $library;
  return confirm_form($form, t('Are you sure you want to delete library %name?', array('%name' => $library['name'])), 'admin/config/system/css-libraries/custom');
}

/**
 * Form submit function for avascript_libraries_delete_form().
 */
function css_libraries_delete_form_submit($form, &$form_state) {
  $library = $form['#library'];
  css_libraries_custom_delete($library['id']);
  drupal_set_message(t('Library %name has been removed', array('%name' => $library['name'])));
  $form_state['redirect'] = 'admin/config/system/css-libraries/custom';
}

/**
 * Form builder function for page callback.
 */
function css_libraries_edit_form($form, &$form_state, $library = array()) {

  $form['#library'] = $library;
  if (!isset($form['#library']['weight'])) {
    $form['#library']['weight'] = 5;
  }

  $form['library_type'] = array(
    '#type' => 'radios',
    '#title' => t('Source'),
    '#required' => TRUE,
    '#options' => array('external' => t('External URL'), 'internal' => t('Internal URL'), 'file' => t('File')),
    '#default_value' => isset($library['type']) ? $library['type'] : 'external',
    '#disabled' => isset($library['type']),
  );

  $external_access = empty($library['type']) || $library['type'] == 'external';
  $form['external_url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#description' => t('Enter the full URL of a css library. URL must start with http:// or https:// and end with .js or .txt.'),
    '#states' => array(
      'visible' => array(
        ':input[name="library_type"]' => array('value' => 'external'),
      ),
    ),
    '#default_value' => isset($library['uri']) ? $library['uri'] : '',
    '#access' => $external_access,
  );


  // $form['cache_external'] = array(
  //   '#type' => 'checkbox',
  //   '#title' => t('Cache script locally'),
  //   '#description' => t('This option only takes effect if css aggregation is enabled. You must verify that the license or terms of service for the script permit local caching and aggregation.'),
  //   '#default_value' => isset($library['cache']) ? $library['cache'] : FALSE,
  //   '#access' => $external_access,
  //   '#states' => array(
  //     'visible' => array(
  //       ':input[name="library_type"]' => array('value' => 'external'),
  //     ),
  //   ),
  // );

  $internal_access = empty($library['type']) || $library['type'] == 'internal';
  $form['internal_url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#description' => t('A local file system path to the stylesheet. URI must start with public:// or private:// and end with .css. '),
    '#states' => array(
      'visible' => array(
        ':input[name="library_type"]' => array('value' => 'internal'),
      ),
    ),
    '#default_value' => isset($library['uri']) ? $library['uri'] : '',
    '#access' => $internal_access,
  );

  $file_access = empty($library['type']) || $library['type'] == 'file';
  $form['js_file_upload'] = array(
    '#type' => 'managed_file',
    '#title' => t('File'),
    '#description' => t('Upload a css file from your computer. It must end in .css.'),
    '#upload_location' => 'public://css_libraries',
    '#upload_validators' => array('file_validate_extensions' => array(0 => 'css')),
    '#states' => array(
      'visible' => array(
        ':input[name="library_type"]' => array('value' => 'file'),
      ),
    ),
    '#default_value' => $file_access && isset($library['fid']) ? $library['fid'] : NULL,
    '#access' => empty($library['type']) || $library['type'] == 'file',
  );

  $form['group'] = array(
    '#type' => 'select',
    '#title' => t('Group'),
    '#required' => TRUE,
    '#options' => _css_libraries_groups() + array('disabled' => t('<Disabled>')),
    '#default_value' => isset($library['group']) ? $library['group'] : CSS_THEME,
  );

  $form['every_page'] = array(
    '#type' => 'checkbox',
    '#title' => t('Every page'),
    '#description' => t('This should be checked if the stylesheet is present on every page of the website.'),
    '#required' => FALSE,
    '#default_value' => isset($library['every_page']) ? $library['every_page'] : TRUE,
  );

  $form['media_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Media types'),
    '#description' => t('The media type for the stylesheet, e.g., all, print, screen.'),
    '#required' => FALSE,
    '#options' => _css_libraries_media_types(),
    '#default_value' => isset($library['media_types']) ? $library['media_types'] : array('all'),
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Library description'),
    '#default_value' => isset($library['name']) ? $library['name'] : '',
    '#description' => 'Defaults to the file name or URL.',
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => 'admin/config/system/css-libraries/custom',
  );

  return $form;
}

/**
 * Form validate for css_libraries_edit_form().
 */
function css_libraries_edit_form_validate($form, &$form_state) {
  switch ($form_state['values']['library_type']) {
    case 'external':
      _css_libraries_url_validate($form, $form_state);
      break;
    case 'internal':
      _css_libraries_path_validate($form, $form_state);
      break;
    case 'file':
      _css_libraries_file_validate($form, $form_state);
      break;
  }
  // Trim the name/description:
  $form_state['values']['name'] = trim($form_state['values']['name']);
}

function _css_libraries_url_validate($form, &$form_state) {
  $form_state['values']['external_url'] = trim($form_state['values']['external_url']);
  if (strlen($form_state['values']['external_url']) == 0) {
    form_set_error('external_url', t('An empty URL is invalid.'));
  }
  elseif (!css_libraries_valid_external_url($form_state['values']['external_url'])) {
    form_set_error('external_url', t('This URL does not start with http:// or does not end with ".js" or ".txt".'));
  }
}

function _css_libraries_path_validate($form, &$form_state) {
  $form_state['values']['internal_url'] = trim($form_state['values']['internal_url']);
  if (strlen($form_state['values']['internal_url']) == 0) {
    form_set_error('internal_url', t('An empty URL is invalid.'));
  }
  elseif (!css_libraries_valid_internal_url($form_state['values']['internal_url'])) {
    form_set_error('internal_url', t('This URL was not recognized as valid system path.'));
  }
}

function _css_libraries_file_validate($form, &$form_state) {
  if (empty($form_state['values']['js_file_upload'])) {
    form_set_error('js_file_upload', 'File field is required when adding a file.');
  }
}

/**
 * Form submit for css_libraries_edit_form().
 */
function css_libraries_edit_form_submit($form, &$form_state) {
  switch ($form_state['values']['library_type']) {
    case 'external':
      if (empty($form['#library']['id'])) {
        // New URL
        $form['#library']['id'] = 'ext-' . db_next_id();
      }
      $custom = variable_get('css_libraries_custom_libraries', array());
      if (strlen($form_state['values']['name']) == 0) {
        $parts = explode('/', $form_state['values']['external_url']);
        $form_state['values']['name'] = '... /' . end($parts);
      }
      $custom[$form['#library']['id']] = array(
        'id' => $form['#library']['id'],
        'type' => 'external',
        'name' => $form_state['values']['name'],
        'group' => $form_state['values']['group'],
        'weight' => $form['#library']['weight'],
        'every_page' => $form_state['values']['every_page'],
        'media_types' => array_keys(array_filter($form_state['values']['media_types'])),
        'uri' => $form_state['values']['external_url'],
        //'cache' => $form_state['values']['cache_external'],
      );
      variable_set('css_libraries_custom_libraries', $custom);
      break;
    case 'internal':
      if (empty($form['#library']['id'])) {
        // New URL
        $form['#library']['id'] = 'int-' . db_next_id();
      }
      $custom = variable_get('css_libraries_custom_libraries', array());
      if (strlen($form_state['values']['name']) == 0) {
        $parts = explode('/', $form_state['values']['internal_url']);
        $form_state['values']['name'] = '... /' . end($parts);
      }
      $custom[$form['#library']['id']] = array(
        'id' => $form['#library']['id'],
        'type' => 'internal',
        'name' => $form_state['values']['name'],
        'group' => $form_state['values']['group'],
        'weight' => $form['#library']['weight'],
        'every_page' => $form_state['values']['every_page'],
        'media_types' => array_keys(array_filter($form_state['values']['media_types'])),
        'uri' => $form_state['values']['internal_url'],
      );
      variable_set('css_libraries_custom_libraries', $custom);
      break;
    case 'file':
      _css_libraries_file_submit($form, $form_state);
      // Change query-strings on css/js files to enforce reload for all users.
      css_libraries_cache_clear();
      break;
  }
  drupal_set_message('Your library has been added. Please configure the region and weight.');
  $form_state['redirect'] = 'admin/config/system/css-libraries/custom';
}

function _css_libraries_file_submit($form, &$form_state) {
  $file = file_load($form_state['values']['js_file_upload']);
  $file->status = FILE_STATUS_PERMANENT;
  // Make the file permanent.
  file_save($file);
  file_usage_add($file, 'css_libraries', 'css_libraries', $file->fid);
  $custom = variable_get('css_libraries_custom_libraries', array());
  $id = 'file-' . $file->fid;
  $custom[$id] = array(
    'type' => 'file',
    'name' => strlen($form_state['values']['name']) ? $form_state['values']['name'] : $file->filename,
    'group' => $form_state['values']['group'],
    'weight' => $form['#library']['weight'],
    'every_page' => $form_state['values']['every_page'],
    'media_types' => array_keys(array_filter($form_state['values']['media_types'])),
    'id' => $id,
    'fid' => $file->fid,
    'uri' => $file->uri,
  );
  variable_set('css_libraries_custom_libraries', $custom);

  if (isset($form['#library']['fid']) && $form['#library']['fid'] != $file->fid) {
    // Replacement file. The id and file have changed, so delete the old one.
    css_libraries_custom_delete($form['#library']['id']);
  }
}

function _css_libraries_groups() {
  return array(CSS_SYSTEM => t('System-layer (CSS_SYSTEM)'), CSS_DEFAULT => t('Module-layer (CSS_DEFAULT)'), CSS_THEME => t('Theme-layer (CSS_THEME)'));
}

function _css_libraries_media_types() {
  return array(
    'all' => t('all: all media type devices. <em>Default</em>.'),
    'aural' => t('aural: speech and sound synthesizers'),
    'braille' => t('braille: braille tactile feedback devices'),
    'embossed' => t('embossed: paged braille printers'),
    'handheld' => t('handheld: small or handheld devices'),
    'print' => t('print: printers'),
    'projection' => t('projection: projected presentations, like slides'),
    'screen' => t('screen: computer screens'),
    'tty' => t('tty: media using a fixed-pitch character grid, like teletypes and terminals'),
    'tv' => t('tv: television-type devices'),
  );
}
